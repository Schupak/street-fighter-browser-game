let attackPower = 0;
let defensePower = 0;

export default function startFight(fightersMap) {
    let fighterEL1 = document.getElementById(`fighter-${Array.from(fightersMap)[0][1].name}`)
    let fighterEL2 = document.getElementById(`fighter-${Array.from(fightersMap)[1][1].name}`)

    let howToUseDiv = document.getElementById("htu")
    howToUseDiv.style.display = "none"
    fighterEL1.setAttribute("style", "transform: translateX(-300px);")
    fighterEL2.setAttribute("style", "transform: translateX(300px) scaleX(1); filter: FlipH;")

    let fighterName1 = fighterEL1.childNodes;
    fighterName1[0].style.display = "none"
    let fighterName2 = fighterEL2.childNodes;
    fighterName2[0].style.display = "none"
    fighterName2[2].style.transform = "scale(-1,1)"

    let fighterInfo1 = Array.from(fightersMap)[0][1];
    let fighterInfo2 = Array.from(fightersMap)[1][1];

    let fightersToHide = document.getElementsByClassName("fighter")
    Array.prototype.forEach.call(fightersToHide, function (el, index) {
        if (fightersToHide[index].id !== fighterEL1.id && fightersToHide[index].id !== fighterEL2.id) {
            fightersToHide[index].style.display = "none"
        }
    })


    let img = new Image();
    let div = document.getElementById('info');
    img.setAttribute("id", "image")
    img.onload = function () {
        div.appendChild(img);
    };
    img.src = '../../resources/fight.png'

    setTimeout(() => {
        showFight(fighterInfo1, fighterInfo2, fighterEL1, fighterEL2)
    }, 2000)
}

function showFight(firstFighterInfo, secondFighterInfo, fighterEL1, fighterEL2) {

    class Fighter {
        constructor(name, health, attack, defense) {
            this.name = name
            this.health = health
            this.attack = attack
            this.defense = defense
        }

        setDamage(damage) {
            this.health -= damage;
        }


        getHitPower(attack) {
            let criticalHitChance = Math.floor(Math.random() * 2) + 1;
            attackPower = attack * criticalHitChance;
            return attackPower
        }

        getBlockPower(defense) {
            let dodgeChance = Math.floor(Math.random() * 2) + 1;
            defensePower = defense * dodgeChance;
            return defensePower
        }

        hit(attackPower, defensePower, enemy) {
            let damage = attackPower - defensePower
            if (damage == 0) {
                let info = document.getElementById("info")
                let block = document.createElement("id", "block")
                block.innerHTML = 'BLOCK'
                info.append(block)
            }
            enemy.setDamage(damage)
        }

    }

    let fighter1 = new Fighter(firstFighterInfo.name, firstFighterInfo.health, firstFighterInfo.attack, firstFighterInfo.defense)
    let fighter2 = new Fighter(secondFighterInfo.name, secondFighterInfo.health, secondFighterInfo.attack, secondFighterInfo.defense)
    let round = 0
    let fightImage = document.getElementById("image")
    fightImage.style.display = "none"



    function fight(fighter1, fighter2) {

        if (!((fighter1.health > 0) && (fighter2.health > 0))) {
            let winner = (fighter1.health > 0) ? fighter1 : fighter2
            let loser = (fighter1.health <= 0) ? fighter1 : fighter2

            return showResult(winner, loser, round);
        }

        round++


        if (round % 2 == 0) {
            fighterEL1.style.left = "68%"
            fighterEL2.style.right = "66%"
            attackPower = fighter1.getHitPower(fighter1.attack)
            defensePower = fighter2.getBlockPower(fighter1.defense)
            // console.log(attackPower, defensePower)
            fighter1.hit(attackPower, defensePower, fighter2)

            fighterEL2.style.backgroundColor = "red"

            showHealth2(fighterEL2, fighter2)

            setTimeout(function () {
                fighterEL1.style.left = "5%"
                fighterEL2.style.right = "5%"
                fighterEL2.style.backgroundColor = "#F6F6F6"
            }, 800)

        } else {
            fighterEL2.style.right = "66%"
            fighterEL1.style.left = "68%"
            attackPower = fighter2.getHitPower(fighter2.attack)
            defensePower = fighter1.getBlockPower(fighter2.defense)
            // console.log(attackPower, defensePower)
            fighter2.hit(attackPower, defensePower, fighter1)
            fighterEL1.style.backgroundColor = "red"
            showHealth1(fighterEL1, fighter1);

            setTimeout(function () {
                fighterEL2.style.right = "5%"
                fighterEL1.style.left = "5%"
                fighterEL1.style.backgroundColor = "#F6F6F6"
            }, 800)

        }

        setTimeout(function () {
            fight(fighter1, fighter2)
        }, 1500)

    }

    setTimeout(function () {
        fight(fighter1, fighter2)
    }, 500)

    function showHealth1(fighterEL, fighter) {
        let hp = document.createElement('span')
        hp.setAttribute("class", "hp")
        hp.style.color = "blue"
        hp.style.transition = "1s";
        hp.style.fontSize = "20px";
        hp.innerHTML = `Health: ${fighter.health}`
        fighterEL.append(hp);
        setTimeout(function () {
            hp.style.display = "none"
        }, 800)
    }

    function showHealth2(fighterEL, fighter) {
        let hp = document.createElement('span')
        hp.setAttribute("class", "hp")
        hp.style.color = "blue"
        hp.style.transition = "1s";
        hp.style.fontSize = "20px";
        // hp.style.transform = "scale(-1,1)"
        hp.innerHTML = `Health: ${fighter.health}`
        fighterEL.append(hp);
        setTimeout(function () {
            hp.style.display = "none"
        }, 800)
    }

    function showResult(winner, loser, round) {
        let result = document.getElementById('info')
        let ko = new Image();
        ko.setAttribute("class", "ko")
        ko.onload = function () {
            result.appendChild(ko);
        };
        ko.src = '../../resources/ko.png'

        let div = document.createElement("h4");
        let textNode = document.createTextNode(`In round  ${round} - ${winner.name} win! Fight Results: 1. ${winner.name}: ${winner.health}hp 2. ${loser.name}: ${loser.health}hp`);
        div.appendChild(textNode)
        result.appendChild(div);
    }




}