import View from './view';

class FighterView extends View {
  constructor(fighter, handleClick, handleDbClick) {
    super();

    this.createFighter(fighter, handleClick, handleDbClick);
  }

  createFighter(fighter, handleClick, handleDbClick) {
    const {
      _id,
      name,
      source
    } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    const createStats = this.createStats();

    const checkBox = this.createElement({
      tagName: 'input',
      className: 'choose',
      attributes: {
        type: 'checkbox',
        id: `fighter-${_id}`,
        name: "check"
      }
    });

    this.element = this.createElement({
      tagName: 'div',
      className: 'fighter'
    });
    this.element.setAttribute("id", `fighter-${fighter.name}`)
    this.element.append(createStats, checkBox, imageElement, nameElement);
    createStats.addEventListener('click', event => handleClick(event, fighter), false);

    this.element.addEventListener('change', event => {

      if (document.querySelector(`#fighter-${_id}`).checked) {
        handleDbClick(event, fighter);
        this.element.style = ' background-color: green; border: #fefefe 5px solid;'
      } else {
        this.element.style = '';
      }
    }, false);
  }

  createName(name) {
    const nameElement = this.createElement({
      tagName: 'div',
      className: 'name'
    });
    nameElement.innerText = name;

    return nameElement;
  }

  createStats() {
    const nameElement = this.createElement({
      tagName: 'div',
      className: 'stats'
    });
    nameElement.setAttribute("id", "stats")
    nameElement.innerText = 'Stats';

    return nameElement;
  }

  createImage(source) {
    const attributes = {
      src: source
    };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }
}

export default FighterView;