import View from './view';
import FighterView from './fighterView';
import {
  fighterService
} from './services/fightersService';
import startFight from './fighter'

let fighterInfo = {}

class FightersView extends View {
  constructor(fighters) {
    super();

    this.handleClick = this.handleFighterClick.bind(this);
    this.handleDbClick = this.handleFighterDbClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick, this.handleDbClick);
      return fighterView.element;
    });

    this.element = this.createElement({
      tagName: 'div',
      className: 'fighters'
    });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {
    fighterInfo = await fighterService.getFighterDetails(fighter._id)

    // let name = fighterInfo.name
    let fighterInfoArr = [fighterInfo.name, fighterInfo.health, fighterInfo.attack, fighterInfo.defense]

    let modal = document.getElementById('myModal')
    let modalContent = document.getElementById("modal-content")
    let nm = document.getElementById('nm')
    let ht = document.getElementById('ht')
    let at = document.getElementById('at')
    let df = document.getElementById('df')
    let inner = [nm.innerHTML = fighterInfoArr[0], ht.innerHTML = fighterInfoArr[1], at.innerHTML = fighterInfoArr[2], df.innerHTML = fighterInfoArr[3]]

    modal.style.display = "block";
    window.onclick = function (event) {
      if (event.target == modal) {
        modal.style.display = "none";
      }
    }
  }

  async handleFighterDbClick(event, fighter) {
    fighterInfo = await fighterService.getFighterDetails(fighter._id)

    if (this.fightersDetailsMap.has(fighter._id)) {
      console.log('already in collection')
      this.fightersDetailsMap.delete(fighter._id)
    } else {
      this.fightersDetailsMap.set(fighter._id, fighterInfo);
    }
    return fighterInfo
  }


}

export default FightersView;