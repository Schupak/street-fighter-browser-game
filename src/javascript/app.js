import FightersView from './fightersView';
import {
  fighterService
} from './services/fightersService';
import startFight from './fighter'

class App {
  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementById('info');
  static button = document.getElementById('button');
  static loadingElement = document.getElementById('loading-overlay');
  // static check1 = document.getElementById(`fighter-${Array.from(FightersView.fightersDetailsMap)[0][1].id}`)


  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';

      const fighters = await fighterService.getFighters();
      const fightersView = new FightersView(fighters);
      const fightersElement = fightersView.element;

      App.rootElement.appendChild(fightersElement);

      button.addEventListener('click', (e) => {
        e.preventDefault();
        if (fightersView.fightersDetailsMap.size === 2) {
          var x = document.getElementsByClassName("choose")[0];
          x.style.display = "none"
          var x1 = document.getElementsByClassName("choose")[1];
          x1.style.display = "none"
          button.style.display = 'none'
          startFight(fightersView.fightersDetailsMap)
        } else {
          alert('You have to choice 2 fighters')
        }
      })
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;