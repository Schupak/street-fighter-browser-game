# stage-2-es6-for-everyone
Test Task 2 **"ES6 for everyone"** for [Binary Studio Academy](https://academy.binary-studio.com/en) 2019.

## Instalation

`npm install`

`npm run build`

`npm run start`

open http://localhost:8080/

## How it works:

![](https://media.giphy.com/media/RfMIh492lftLaPdRQ4/giphy.gif)
